/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetablefactory;

public class Beet extends Vegetable {
    
   public Beet(String name, double size, String colour){
       super(name, size, colour);
   }
   
   public Beet(double size, String colour){
       this.size = size;
       this.colour = colour;
   }
   
   public String isRipe(){
       
       if(getSize() == 2 && getColour().equalsIgnoreCase("Red")){
           
            return "Beet is ripe.";
       }
       
       return "Beet is not ripe.";
   }
    
}

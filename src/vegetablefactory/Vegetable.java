/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetablefactory;

public abstract class Vegetable {
    
    protected String colour;
    protected double size;
    protected String name;
    
    public Vegetable(){}
    
    public Vegetable(String name, double size, String colour){
        
        this.name = name;
        this.size = size;
        this.colour = colour;
    }
    
    public  String getColour(){
        return colour;
    }
   
    public double getSize(){
        return size;
    }
    
    public String getName(){
        return name;
    }
    
    public abstract String isRipe( );
    
}

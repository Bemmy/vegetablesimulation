/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetablefactory;

public class VegetableFactory {

    private static VegetableFactory vegFactory;
    
    private VegetableFactory(){}
    
    public static VegetableFactory getInstance(){
        
        if(vegFactory == null){
            
            vegFactory = new VegetableFactory();
        }
                    return vegFactory; 
    }
    
    public Vegetable getVegetable(VegetableTypes type, String color, double size ){
        
        Vegetable veggie = null;
        
        switch( type){
            case CARROT: veggie = new Carrot(size, color);
            break;
            case BEET: veggie = new Beet(size, color);
            break;
        }    
        return veggie;
    }
   
}
        
    
    


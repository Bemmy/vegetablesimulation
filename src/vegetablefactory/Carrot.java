/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetablefactory;

public class Carrot extends Vegetable {
    
    
    
  public Carrot(double size, String colour){
      this.size = size;
      this.colour = colour;
  }
    
    public String isRipe( ){
        
      if(this.size == 1.5 && this.colour.equalsIgnoreCase("Orange")){
           
            return "Carrot is ripe.";
       }
       
       return "Carrot is not ripe.";
   }
}

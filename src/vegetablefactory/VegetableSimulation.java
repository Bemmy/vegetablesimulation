/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetablefactory;

public class VegetableSimulation {
    
    public static void main(String[] args){
        
    VegetableFactory factory = VegetableFactory.getInstance();
    
    Vegetable carrot1 = (Carrot) (factory.getVegetable(VegetableTypes.CARROT, "Orange", 1.5 ));
    Vegetable carrot2 = (Carrot) (factory.getVegetable(VegetableTypes.CARROT, "Yellow", 1 ));
    
    Vegetable beet1 = (Beet)(factory.getVegetable(VegetableTypes.BEET, "Red", 2 ));
    Vegetable beet2 = (Beet)(factory.getVegetable(VegetableTypes.BEET, "Pink", 1.5 ));
    
    System.out.println(carrot1.isRipe());
    System.out.println(carrot2.isRipe());
    
    System.out.println(beet1.isRipe());
    System.out.println(beet2.isRipe());
    
}
}